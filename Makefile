# Makefile for latex slides

TARGET_SOURCES=$(wildcard **/*.tex)
TARGET=$(subst .tex,,$(TARGET_SOURCES))
TARGET_DIRECTORIES=$(shell find -maxdepth 2 -type l -name 'Makefile' | sed 's/\.\///' | grep -E '^[0-9]\.[0-9]' | awk -F / '{print $$1}')
TARGET_PDFS=$(subst .tex,.pdf,$(TARGET_SOURCES))

.PHONY: all
all: $(TARGET)

.PHONY: clean
clean:
	for D in $(TARGET_DIRECTORIES) ; do make -C $$D clean; done

%:
	make -C $(dir $@) all
